#include <iostream>
#include <ctype.h>
#include <chrono>
#include <thread>

// Funktioniert nicht mit GetPot!:
// #include <windows.h>

#include "GetPot"

using namespace std;

void print_help(const string);
static inline std::string string_lower(const std::string &str);
static inline void SleepRandom(int sleep, int variancePercentMax);


// Config
static int SleepSefaultMs = 200;
static int SleepVariancePercent = 250;


int main(int argc, char *argv[], char *envp[]) {
    // SetConsoleOutputCP(65001);
    cout << "Toms Windows / Console Stdout / StdErr / Exit Code Generator" << endl;
    cout << "002, 220114, tom@jig.ch" << endl;
    cout << endl;

    srand (time(NULL));

    // Die Argumente einlesen
    // http://getpot.sourceforge.net/html/index.html
    GetPot   cl(argc, argv);

    if( cl.size() == 1 || cl.search(2, "--help", "-h") ) print_help(cl[0]);

    // Wird ein Rückgabewert vorgegeben?
    int  ExitCode = 0;
    if( cl.search(2, "--exit", "-x") ) {
        ExitCode = cl.next(0.);
    }

    int  SleepMs = SleepSefaultMs;
    if( cl.search(2, "--sleep", "-s") ) {
        SleepMs = cl.next(0.);
    }

    // Sollten die Env-Variablen angezeigt werden?
    bool showEnvData = false;
    if( cl.search(2, "--showenv", "-e") ) {
        showEnvData = true;
    }

    // Den Feed mit den StdOut / StdErr lesen
    // e.g.
    //  s e s2 e5
    //  Erzeugt
    //      StrdOut
    //      StrdErr
    //      StrdOut 2x
    //      StrdErr 5x
    const vector<string> stdInErrFeed = cl.nominus_followers(2, "-f", "--feed");


    // Walk through list of strings until a NULL is encountered.
    if (showEnvData) {
        cout << endl << endl;
        cout << "Environment Data: " << endl;
        bool showLineNumbers = true;
        for (int i = 0; envp[i] != NULL; ++i ) {
            if (showLineNumbers) cout << i << ": ";
            cout << envp[i] << "\n";
        }
        cout << endl << endl;
    }


    // Den StdIn / StdErr Feed verarbeiten
    if (stdInErrFeed.size() > 0) {
        int cntCout = 0;
        int cntCerr = 0;
        for (auto &feed: stdInErrFeed) {
            // Debug: Das aktuelle Element ausgeben
            // std::cout << feed << std::endl;
            string feedLc = string_lower(feed);

            // Das Präfix lesen
            std::string prefix = feedLc.substr(0,1);

            // Den Multiplikator bestimmen
            int numbers = 1;
            // Mehr als 1 Zeichen?
            if (feed.size() > 1) {
                std::string sNumbers = feed.substr (1);
                std::istringstream (sNumbers) >> numbers;
            }

            // Debug
            // cout << "     " << prefix << endl;
            // cout << "     " << numbers << endl;

            switch(prefix.at(0)) {
                case 's':
                    for(int i = 1; i <= numbers; ++i) {
                        cout << "cout (" << cntCout++ << ")" << endl;
                        SleepRandom(SleepMs, SleepVariancePercent);
                    }
                    break;
                case 'e':
                    for(int i = 1; i <= numbers; ++i) {
                        cerr << "***  cerr (" << cntCerr++ << ")" << endl;
                        SleepRandom(SleepMs, SleepVariancePercent);
                    }
                    break;
                default:
                    cout << "Fehler, unbekanntes Präfix: " << prefix.at(0) << endl;
                    break;
            }

        }

    }

    cout << "Setze Exit Code: " << ExitCode << endl;
    return ExitCode;
}


static inline void SleepRandom(int sleep, int variancePercentMax) {
    // Mind. 10ms
    sleep = std::max(10, sleep);
    // Mind. 20%
    variancePercentMax = std::max(20, variancePercentMax);
    int variancePercent = rand() % variancePercentMax;
    int sleepRes = sleep + sleep / 100 * variancePercent;
    std::this_thread::sleep_for(std::chrono::milliseconds(sleepRes));
}

static inline char string_tolower_functional(char c) {
    return std::tolower(c);
}


static inline std::string string_lower(const std::string &str) {
    std::string strcopy(str.size(), 0);
    std::transform(str.begin(), str.end(), strcopy.begin(), string_tolower_functional);
    return strcopy;
}


void print_help(const string Application) {
    cout << endl;
    cout << "!Ex" << endl;
    cout << "win-stdout-stderr-generator.exe -x 9 -s 80 -f s10 e5 s15" << endl;
    cout << " Exit Code 9" << endl;
    cout << " 80ms warten" << endl;
    cout << " 10x StdOut" << endl;
    cout << "  5x StdErr" << endl;
    cout << " 15x StdOut" << endl;
    cout << endl;
    cout << "Usage:" << endl;
    cout << "-h --help  get this help" << endl;
    cout << endl;
    cout << "-x --exit N      Exit Code" << endl;
    cout << "-e --showenv     Zeigt die User Systemumgebung an" << endl;
    cout << "-f --feed        Den zu erzeugenden StdOut / StdErr feed" << endl;
    cout << "                   S = StdOut" << endl;
    cout << "                   E = StdErr" << endl;
    cout << "               " << endl;
    cout << "                 Muster: S<Multiplikator> | S<Multiplikator>" << endl;
    cout << "                   Ex." << endl;
    cout << "                     -f S E" << endl;
    cout << "                     -f s10 e10 s20" << endl;
    cout << "-s --sleep Nms   Wie viele ms soll zwischen StdOut /StdErr" << endl;
    cout << "                 Ausgaben geschlafen werden?" << endl;
    cout << "                 (Wird zufällig variert)" << endl;
    cout << endl;
    exit(0);
}

